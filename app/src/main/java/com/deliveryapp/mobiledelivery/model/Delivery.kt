package com.deliveryapp.mobiledelivery.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Delivery(
        val imageUrl: String = "",
        val description: String = "",
        val location: Location = Location()
) : Parcelable