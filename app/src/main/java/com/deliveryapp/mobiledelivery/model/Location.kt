package com.deliveryapp.mobiledelivery.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(
        val address: String = "",
        val lng: Double = 0.0,
        val lat: Double = 0.0
) : Parcelable