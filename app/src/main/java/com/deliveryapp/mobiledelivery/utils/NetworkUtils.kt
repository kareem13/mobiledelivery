package com.deliveryapp.mobiledelivery.utils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Check whether the device is connected to internet or not
 */

fun Context.isOnline(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = cm.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnected
}

