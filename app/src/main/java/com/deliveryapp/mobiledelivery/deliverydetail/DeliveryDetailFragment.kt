package com.deliveryapp.mobiledelivery.deliverydetail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.deliveryapp.mobiledelivery.R
import com.deliveryapp.mobiledelivery.model.Delivery
import com.deliveryapp.mobiledelivery.utils.Constants
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_delivery_detail.*
import kotlinx.android.synthetic.main.row_todeliver.*


/**
 * A fragment representing a single Delivery detail screen.
 * This fragment is either contained in a [DeliveryListActivity]
 * in two-pane mode (on tablets) or a [DeliveryDetailActivity]
 * on handsets.
 */
class DeliveryDetailFragment : Fragment() {
    private lateinit var delivery: Delivery
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(Constants.DELIVERY_DATA))
                delivery = it.getParcelable(Constants.DELIVERY_DATA)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_delivery_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mapView.onCreate(savedInstanceState)
        mapView.onResume()

        try {
            MapsInitializer.initialize(context)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        deliverToText.text = delivery.description
        locationText.text = delivery.location.address
        Glide.with(this).load(delivery.imageUrl).into(profileImage)

        mapView.getMapAsync {
            val deliveryLocation = delivery.location
            val latlng = LatLng(deliveryLocation.lat, deliveryLocation.lng)
            // create marker
            val marker = MarkerOptions().position(latlng)

            // Changing marker icon
            marker.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_ROSE))

            // adding marker
            it.addMarker(marker)
            val cameraPosition = CameraPosition
                    .Builder()
                    .target(latlng)
                    .zoom(20f)
                    .tilt(50f)
                    .build()
            it.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition))
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        if (mapView != null) {
            mapView.onDestroy()
        }
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }
}
