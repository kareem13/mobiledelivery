package com.deliveryapp.mobiledelivery.deliverydetail

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.deliveryapp.mobiledelivery.R
import com.deliveryapp.mobiledelivery.delivery.DeliveryListActivity
import com.deliveryapp.mobiledelivery.utils.Constants
import kotlinx.android.synthetic.main.activity_delivery_detail.*

/**
 * An activity representing a single Delivery detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [DeliveryListActivity].
 */
class DeliveryDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_detail)
        setSupportActionBar(detail_toolbar)

        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            val fragment = DeliveryDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(Constants.DELIVERY_DATA, intent.getParcelableExtra(Constants.DELIVERY_DATA))
                }
            }

            supportFragmentManager.beginTransaction()
                    .add(R.id.deliveryDetailContainer, fragment)
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    navigateUpTo(Intent(this, DeliveryListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
}
