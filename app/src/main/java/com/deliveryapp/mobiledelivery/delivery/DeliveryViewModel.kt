package com.deliveryapp.mobiledelivery.delivery

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.deliveryapp.mobiledelivery.model.Delivery
import com.deliveryapp.mobiledelivery.network.DeliveryApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class DeliveryViewModel(application: Application) : AndroidViewModel(application) {
    private val deliveryList: MutableLiveData<List<Delivery>> = MutableLiveData()
    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isError: MutableLiveData<Throwable> = MutableLiveData()


    fun getDeliveries(): MutableLiveData<List<Delivery>> {
        loadDeliveries()
        return deliveryList
    }

    private fun loadDeliveries() {
        val repository = DeliveryApiService.create(getApplication())
        repository.getDeliveries()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                // server randomly return 500 error so we could retry till we get data.
                .retry(3L)
                .subscribe(
                        {
                            deliveryList.value = it
                            isLoading.value = false
                        },
                        {
                            isLoading.value = false
                            isError.value = it
                        }
                )
    }
}
