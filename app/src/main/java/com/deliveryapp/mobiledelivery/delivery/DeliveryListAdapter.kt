package com.deliveryapp.mobiledelivery.delivery

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.deliveryapp.mobiledelivery.R
import com.deliveryapp.mobiledelivery.model.Delivery
import kotlinx.android.synthetic.main.row_todeliver.view.*


class DeliveryListAdapter(
        private val context: Context,
        private var data: List<Delivery> = arrayListOf()
) : RecyclerView.Adapter<DeliveryListAdapter.MyViewHolder>() {

    companion object {
        lateinit var clickListener: ClickListener
    }

    interface ClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        val profileImage = view.profileImage
        val deliverText = view.deliverToText
        val locationText = view.locationText

        override fun onClick(v: View) {
            clickListener.onItemClicked(adapterPosition, v)
        }

    }

    fun setOnItemClick(clickListener: ClickListener) {
        Companion.clickListener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_todeliver, parent, false)

        return MyViewHolder(itemView)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val delivery = data[position]
        holder.deliverText.text = delivery.description
        holder.locationText.text = delivery.location.address
        Glide.with(context).load(delivery.imageUrl).into(holder.profileImage)
    }

    fun setDataList(deliveryList: List<Delivery>) {
        data = deliveryList
    }
}