package com.deliveryapp.mobiledelivery.delivery

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.deliveryapp.mobiledelivery.R
import com.deliveryapp.mobiledelivery.deliverydetail.DeliveryDetailActivity
import com.deliveryapp.mobiledelivery.deliverydetail.DeliveryDetailFragment
import com.deliveryapp.mobiledelivery.model.Delivery
import com.deliveryapp.mobiledelivery.utils.Constants
import com.deliveryapp.mobiledelivery.utils.isOnline
import kotlinx.android.synthetic.main.activity_delivery_list.*

/**
 * An activity representing a list of Deleveries. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of deleveries, which when touched,
 * lead to a [DeliveryDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class DeliveryListActivity : AppCompatActivity() {
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var isTabletDevice: Boolean = false
    private lateinit var viewModel: DeliveryViewModel
    private lateinit var adapter: DeliveryListAdapter
    private lateinit var deliveries: List<Delivery>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_list)

        viewModel = ViewModelProviders.of(this).get(DeliveryViewModel::class.java)
        adapter = DeliveryListAdapter(applicationContext)

        populateDeliveryList()

        // add swipe refresh to recyclerview
        swipeRefresh.setOnRefreshListener {
            if (applicationContext.isOnline()) {
                populateDeliveryList()
            }
            else {
                Snackbar.make(coordinator, R.string.message_nointernet, Snackbar.LENGTH_LONG).show()
                swipeRefresh.isRefreshing = false
            }
        }

        // show error snackbar when the device is offline
        viewModel.isError.observe(this, Observer { error ->
            Snackbar.make(coordinator, R.string.message_nointernet, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.notification_retry)
                    {
                        progressBar.visibility = View.VISIBLE
                        populateDeliveryList()
                    }
                    .show()
        })

        // show progress bar when loading list
        viewModel.isLoading.observe(this, Observer {
            it?.let { loading ->
                if (loading) {
                    progressBar.visibility = View.VISIBLE
                } else {
                    progressBar.visibility = View.INVISIBLE
                }
            }
        })

        adapter.setOnItemClick(
                object : DeliveryListAdapter.ClickListener {
                    override fun onItemClicked(position: Int, view: View) {
                        if (isTabletDevice) {
                            // in case the device is tablet, show the fragment
                            val fragment = DeliveryDetailFragment().apply {
                                arguments = Bundle().apply {
                                    putParcelable(Constants.DELIVERY_DATA, deliveries[position])
                                }
                            }
                            emptyFragmentMessage?.visibility = View.INVISIBLE
                            //start fragment transaction
                            supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.deliveryDetailContainer, fragment)
                                    .commit()
                        } else {
                            // in case the device is phone, send user to another activity with details
                            val intent = Intent(applicationContext, DeliveryDetailActivity::class.java).apply {
                                putExtra(Constants.DELIVERY_DATA, deliveries[position])
                            }
                            startActivity(intent)
                        }
                    }
                })

        if (deliveryDetailContainer != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in tablet mode.
            isTabletDevice = true
        }
    }

    /**
     * populate recyclerview with delivereis list from viewmodel that consume rest api.
     * support refresh layout
     */
    private fun populateDeliveryList() {
        viewModel.getDeliveries().observe(this, Observer {
            if (it != null) {
                adapter.setDataList(it)
                swipeRefresh.isRefreshing = false
                deliveryList.adapter = adapter
                deliveries = it
            }
        })
    }
}
