package com.deliveryapp.mobiledelivery.network

import android.content.Context
import com.deliveryapp.mobiledelivery.model.Delivery
import com.deliveryapp.mobiledelivery.utils.isOnline
import io.reactivex.Observable
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import java.io.File
import java.util.concurrent.TimeUnit


interface DeliveryApiService {
    @GET("/deliveries")
    fun getDeliveries(): Observable<List<Delivery>>

    companion object {
        /**
         * create retrofit instance to contact the api
         */


        fun create(context: Context): DeliveryApiService {
            val cacheSize = 10 * 1024 * 1024 // 10 MB
            val httpCacheDirectory = File(context.cacheDir, "responses")
            val cache = Cache(httpCacheDirectory, cacheSize.toLong())

            /*
            create an interceptor that will be used in each request in order to cache the response
            in case there is no internet connection (max cache is 7 days)
             */
            val offlineCacheInterceptor = Interceptor {
                var request = it.request()
                if (!context.isOnline()) {
                    val cacheControl = CacheControl.Builder()
                            .maxStale(7, TimeUnit.DAYS)
                            .build()

                    request = request.newBuilder()
                            .cacheControl(cacheControl)
                            .build()
                }
                return@Interceptor it.proceed(request)

            }

            // create a network cache interceptor, setting the max age to 2 minute
            val networkCacheInterceptor = Interceptor {
                val response = it.proceed(it.request())
                val cacheControl = CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES)
                        .build()

                return@Interceptor response.newBuilder()
                        .header("cache-control", cacheControl.toString())
                        .build()
            }

            // Create the logging interceptor to debug any network problem
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            // Create ok http client instance
            val httpClient = OkHttpClient.Builder()
                    .cache(cache)
                    .addInterceptor(offlineCacheInterceptor)
                    .addNetworkInterceptor(networkCacheInterceptor)
                    .addInterceptor(loggingInterceptor)
                    .build()

            // Finaly retrofit instance that will contact the api
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(MoshiConverterFactory.create())
                    //10.0.2.2 represent localhost in android emulator
                    .baseUrl("http://10.0.2.2:8080/")
                    .client(httpClient)
                    .build()

            return retrofit.create<DeliveryApiService>(DeliveryApiService::class.java)
        }
    }
}